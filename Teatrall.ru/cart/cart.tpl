<div class="container">
    <div class="row">
        <div class="col-lg-18 col-lg-offset-3 col-md-24 col-sm-24">
            <ul class="breadcrumbs breadcrumbs--steps">
                <li class="breadcrumbs__item">
                    <span class="breadcrumbs__item__text">1. Выбор места</span>
                </li>
                <li class="breadcrumbs__item">
                    <span class="breadcrumbs__item__text breadcrumbs__item__text--red">2. Оформление заказа</span>
                </li>
                <li class="breadcrumbs__item">
                        <span class="breadcrumbs__item__text breadcrumbs__item__text--gray">3. Оплата
                        </span>
                </li>
                <li class="breadcrumbs__item">
                        <span class="breadcrumbs__item__text breadcrumbs__item__text--gray">4. Подтверждение заказа
                        </span>
                </li>
                <li class="breadcrumbs__line">
                    <hr/>
                </li>
            </ul>
            <!-- <div class="row">
                <div class="col-lg-10 col-md-10 col-xs-24">
                    <div class="discount">
                        <div class="discount__media discount__media--icon">
                            ç
                        </div>
                        <div class="discount__text">
                            Билеты без сервисного сбора до 15 марта
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-xs-24">
                    <div class="discount">
                        <div class="discount__media discount__media--icon">
                            $
                        </div>
                        <div class="discount__text">
                            Билеты без сервисного сбора до 15 марта
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-xs-24">
                    <div class="discount">
                        <div class="discount__media discount__media--icon">
                            !!!
                        </div>
                        <div class="discount__text">
                            Билеты без сервисного сбора до 15 марта
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs 12">
                    <div class="discount">
                        <div class="discount__media discount__media--avatar">
                            <img src="">
                        </div>
                        <div class="discount__text">
                            <div class="discount__text--name">
                                User_Name
                            </div>
                            Ваша персональная скидка 5%
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row booking relative" ng-if="BookingCartCtrl.showCart()">
                <div class="watch">
                    <p>Бронь билетов
                        истекает через</p>
                    <span class="watch_countdown">{{ BookingCartCtrl.counter | countdown }}</span>
                </div>
                <div ng-repeat="schedule in BookingCartCtrl.cart.schedules"
                     ng-if="schedule.tickets.length > 0"
                     card-booking-block="schedule"></div>
                <div>
                    <table class="table table--total table--total--fix-width">
                        <tbody>
                            <tr>
                                <td colspan="4">Билеты:</td>
                                <td class="ruble">{{ BookingCartCtrl.calculateTicketsPrice() }}</td>
                            </tr>
                            <tr>
                                <td colspan="4">Сервисный сбор:</td>
                                <td  class="ruble">{{ BookingCartCtrl.calculateTicketsFee() }}</td>
                            </tr>
                            <tr class="table__total">
                                <td colspan="4">За {{ BookingCartCtrl.eventsCount() }} {{ 'EVENT' | pluralize: BookingCartCtrl.eventsCount() }}:</td>
                                <td>{{ BookingCartCtrl.calculateTotalPrice() }}<span class="ruble"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div ng-if="BookingCartCtrl.showCart()">
    <section>
        <ul class="tabs navigation__right">
            <li class="tabs__item tabs__item--border-blue tabs__item--active">Оформление заказа</li>
        </ul>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-lg-18 col-lg-offset-3 col-md-24 col-sm-24 mt-lg">
                <div ng-form="orderForm" class="row ordering" ng-if="BookingCartCtrl.cart">
                    <!--div class="col-lg-24 col-md-24 col-xs-24">
                        <p>Для получения информации по распродажам билетов <br> и сохранения истории заказов <a ng-click="MainCtrl.openRegistrationModal()">зарегистрируйтесь</a> или <a ng-click="MainCtrl.openAuthModal()"">авторизируйтесь</a></p>
                    </div-->
                    <div class="col-lg-8 col-md-8 col-xs-24">
                        <div class="ordering__parameters">
                            <p class="ordering__p mb-sm ">Способ получения билетов:</p>
                            <button class="button button--large_text button--large button--fluid mb-sm"
                                    ng-class="{'button--active': BookingCartCtrl.order.deliveryType == 'pickup'}"
                                    ng-click="BookingCartCtrl.order.deliveryType = 'pickup'">
                                самовывоз
                            </button>
                            <button class="button button--large_text button--large button--fluid mb-sm"
                                    ng-class="{'button--active': BookingCartCtrl.order.deliveryType.indexOf('courier') >= 0}"
                                    ng-click="BookingCartCtrl.order.deliveryType = 'courier_moscow'">
                                доставка
                            </button>
                            <button class="button button--large_text button--large button--fluid mb-sm"
                                    ng-class="{'button--active': BookingCartCtrl.order.deliveryType == 'eticket'}"
                                    ng-click="BookingCartCtrl.order.deliveryType = 'eticket'; BookingCartCtrl.order.paymentType = 'card'"
                                    ng-disabled="!BookingCartCtrl.isEticketPossible()">
                                электронный билет
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-24">
                        <div class="ordering__parameters">
                            <p ng-if="BookingCartCtrl.order.deliveryType == 'pickup'" class="ordering__p mb-sm">Данные самовывоза:</p>
                            <p ng-if="BookingCartCtrl.order.deliveryType.indexOf('courier') >= 0" class="ordering__p mb-sm">Данные доставки:</p>
                            <p ng-if="BookingCartCtrl.order.deliveryType == 'eticket'" class="ordering__p mb-sm">Данные электронного билета:</p>
                            <div ng-if="BookingCartCtrl.order.deliveryType.indexOf('courier') >= 0" class="input-group  input-group--large input-group--normal_text mb-sm  ordering__input input-group--select">
                                <select class="input" ng-model="BookingCartCtrl.order.deliveryType">
                                    <option value="courier_moscow">Районы Москвы в пределах МКАД</option>
                                    <option value="courier_new_moscow">Районы Москвы за МКАД</option>
                                    <option value="courier_country">Ближайшее Подмосковье</option>
                                </select>
                            </div>
                            <div ng-if="BookingCartCtrl.order.deliveryType.indexOf('courier') >= 0"
                                 class="input-group input-group--large input-group--normal_text mb-sm ordering__input"
                                 ng-class="{'input__false': orderForm.address.$invalid, 'input__true': orderForm.address.$valid }">
                                <input name="address"
                                       ng-model="BookingCartCtrl.order.address"
                                       type="text"
                                       class="input"
                                       placeholder="Адрес"
                                       required>
                            </div>
                            <!-- <div class="input-group input-group--half input-group--large input-group--normal_text mb-sm input-group--select ordering__input ">
                                <select class="input">
                                    <option disabled selected>Дата</option>
                                    <option>Сегодня</option>
                                    <option>Завтра</option>
                                    <option>Послезавтра</option>
                                </select>
                            </div>
                            <div class="input-group input-group--half input-group--large input-group--normal_text mb-sm input-group--select ordering__input">
                                <select class="input">
                                    <option disabled selected>Время</option>
                                    <option>12:00</option>
                                    <option>12:30</option>
                                    <option>13:00</option>
                                    <option>13:30</option>
                                    <option>14:00</option>
                                    <option>14:30</option>
                                </select>
                            </div> -->
                            <div ng-if="BookingCartCtrl.order.deliveryType != 'eticket'"
                                 class="input-group input-group--large input-group--normal_text mb-sm ordering__input"
                                 ng-class="{'input__true': BookingCartCtrl.order.fullName.length > 0 }">
                                <input type="text" class="input" ng-model="BookingCartCtrl.order.fullName" placeholder="Контактное лицо">
                            </div>
                            <div ng-if="BookingCartCtrl.order.deliveryType == 'eticket'"
                                 class="input-group input-group--large input-group--normal_text mb-sm input__true ordering__input"
                                 ng-class="{'input__false': orderForm.lastName.$invalid, 'input__true': orderForm.lastName.$valid }">
                                <input name="lastName"
                                       type="text"
                                       ng-model="BookingCartCtrl.order.lastName"
                                       class="input"
                                       placeholder="Фамилия"
                                       required>
                            </div>
                            <div ng-if="BookingCartCtrl.order.deliveryType == 'eticket'"
                                 class="input-group input-group--large input-group--normal_text mb-sm input__true ordering__input"
                                 ng-class="{'input__false': orderForm.firstName.$invalid, 'input__true': orderForm.firstName.$valid }">
                                <input name="firstName"
                                       type="text"
                                       ng-model="BookingCartCtrl.order.firstName"
                                       class="input"
                                       placeholder="Имя"
                                       required>
                            </div>
                            <div class="input-group input-group--large input-group--normal_text mb-sm ordering__input"
                                 ng-class="{'input__false': orderForm.phone.$invalid || BookingCartCtrl.existsUser}">
                                <input name="phone"
                                       ng-model="BookingCartCtrl.order.phone"
                                       type="tel"
                                       ng-change="BookingCartCtrl.existsUser = false;"
                                       class="input"
                                       ui-mask="+7 (999) 999 99 99"
                                       required>
                            </div>
                            <div ng-if="BookingCartCtrl.order.deliveryType != 'eticket'"
                                 class="input-group input-group--large input-group--normal_text mb-sm ordering__input"
                                 ng-class="{'input__true': BookingCartCtrl.order.email && orderForm.email.$valid, 'input__false': BookingCartCtrl.existsUser}">
                                <input name="email"
                                       type="email"
                                       class="input"
                                       ng-change="BookingCartCtrl.existsUser = false;"
                                       ui-validate="{valid :'BookingCartCtrl.validateNotRequiredEmail($value)'}"
                                       ng-model="BookingCartCtrl.order.email" placeholder="Почта (не обязательно)">
                            </div>
                            <div ng-if="BookingCartCtrl.order.deliveryType == 'eticket'"
                                 class="input-group input-group--large input-group--normal_text mb-sm input__true ordering__input"
                                 ng-class="{'input__true': BookingCartCtrl.order.email && orderForm.email.$valid, 'input__false': orderForm.email.$invalid || BookingCartCtrl.existsUser}"
                                 >
                                <input name="email"
                                       type="email"
                                       class="input"
                                       ng-change="BookingCartCtrl.existsUser = false;"
                                       ui-validate="{valid :'BookingCartCtrl.validateRequiredEmail($value)'}"
                                       ng-model="BookingCartCtrl.order.email" placeholder="Почта"
                                       required>
                            </div>
                            <address ng-if="BookingCartCtrl.order.deliveryType == 'pickup'"><i class="icon-map-marker- link--blue"></i> Адрес кассы: Пушкарев переулок, д. 9 м. Трубная, м. Сухаревская</address>
                            <div ng-show="BookingCartCtrl.showErrors">
                                <p class="error_message__p" ng-if="orderForm.address.$error.required">Вы не ввели адрес</p>
                                <p class="error_message__p" ng-if="orderForm.lastName.$error.required">Вы не ввели фамилию</p>
                                <p class="error_message__p" ng-if="orderForm.firstName.$error.required">Вы не ввели имя</p>
                                <p class="error_message__p" ng-if="orderForm.phone.$error.required">Вы не ввели телефон</p>
                                <p class="error_message__p" ng-if="orderForm.email.$error.required">Вы не ввели email</p>
                                <p class="error_message__p" ng-if="orderForm.email.$error.valid && !orderForm.email.$error.required">Вы ввели неверный email</p>
                                <p class="error_message__p" ng-if="BookingCartCtrl.existsUser">Введенные email или телефон уже использутся. Вы можете <a ng-click="BookingCartCtrl.signIn()">войти</a> или <a ng-click="BookingCartCtrl.registration()">зарегистрироваться</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-24">
                        <div class="ordering__parameters mb-lg">
                            <p class="ordering__p mb-sm ">Способ оплаты:</p>
                            <button class="button button--large_text button--large button--fluid mb-sm"
                                    ng-class="{'button--active': BookingCartCtrl.order.paymentType == 'post_cash'}"
                                    ng-click="BookingCartCtrl.order.paymentType = 'post_cash'"
                                    ng-disabled="BookingCartCtrl.order.deliveryType == 'eticket'">
                                наличные
                            </button>
                            <button class="button button--large_text button--large button--fluid mb-sm"
                                    ng-class="{'button--active': BookingCartCtrl.order.paymentType == 'card'}"
                                    ng-click="BookingCartCtrl.order.paymentType = 'card'"
                                    ng-disabled="!BookingCartCtrl.isCardPaymentPossible()">
                                картой
                            </button>
                            <div ng-disabled="orderForm.$invalid"
                                 ng-click="BookingCartCtrl.showErrors = true">
                                <div class="ordering__total ordering__total--card">
                                    <div ng-class="{'ordering__total_overlay': !orderForm.$valid}"></div>
                                    <!-- <div class="input-group input-group--half input-group--large input-group--normal_text mb-sm ordering__input ordering__input--promo">
                                        <input type="text" class="input" placeholder="Промо-код">
                                    </div>
                                    <div class="input-group input-group--half input-group--large input-group--normal_text mb-sm ordering__input promo">
                                        <button class="button  button--large button--fluid button--blue promo__show">активировать</button>
                                        <p class="promo__hide promo__true ruble">Скидка: 350</p>
                                        <p class="promo__hide promo__false">попробуйте еще раз</p>
                                    </div> -->
                                    <div class="ordering__total__content">
                                        <p class="ordering__total__content__text ruble">За {{ BookingCartCtrl.eventsCount() }} {{ 'EVENT' | pluralize: BookingCartCtrl.eventsCount() }}: {{ BookingCartCtrl.calculateTotalPrice() }}</p>
                                        <p ng-if="BookingCartCtrl.order.deliveryType.indexOf('courier') >= 0" class="ordering__total__content__text ruble">{{ BookingCartCtrl.deliveryTypeInfo[BookingCartCtrl.order.deliveryType].info }}: {{ BookingCartCtrl.deliveryTypeInfo[BookingCartCtrl.order.deliveryType].price }}</p>
                                        <p class="ordering__total__content__text ordering__total__content__text--bold ruble">Сумма заказа: {{ BookingCartCtrl.calculateOrderPrice() }}</p>
                                    </div>
                                </div>
                                <button class="button ordering__total__button  button--large button--red button--large_text"
                                        ng-disabled="orderForm.$invalid"
                                        ng-click="BookingCartCtrl.createOrder()">
                                    <span ng-if="BookingCartCtrl.order.paymentType == 'card'">оплатить</span>
                                    <span ng-if="BookingCartCtrl.order.paymentType == 'post_cash'">оформить</span>
                                </button>
                            </div>
                            <div class="master_cart">
                                <p class="ordering__p ng-scope">Принимаеем к оплате:</p>
                                <ul>
                                    <li class="ordering__p">наличные</li>
                                    <li class="ordering__p">банковские карты</li>
                                </ul>
                                <img src="/1.0.0/images/cards.png">
                            </div>
                            <small>
                                Нажимая на кнопку <span ng-if="BookingCartCtrl.order.paymentType == 'card'">«оплатить»</span> <span ng-if="BookingCartCtrl.order.paymentType == 'post_cash'">«оформить»</span>, вы подтверждаете свое согласие с условиями предоставления услуг <a ui-sref="main.personal.offer" target="_blank"> (пользовательское соглашение)</a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
