goog.provide('Teatrall.site.states.main.tickets.booking.cart.ctrl');

goog.require('libs.angular');
goog.require('libs.angular.ui.router');
goog.require('libs.angular.localstorage');
goog.require('libs.angular.metrika');
goog.require('libs.sourcebuster');

goog.require('Teatrall.core.utils.misc');

goog.require('Teatrall.core.utils.signal');
goog.require('Teatrall.core.utils.scroll');
goog.require('Teatrall.core.utils.ab');
goog.require('Teatrall.core.models.cart');
goog.require('Teatrall.core.models.analyzableTicket');
goog.require('Teatrall.core.models.order');


(function (angular, Teatrall, sbjs) {
    // 'use strict';

    Teatrall.site.states.main.tickets.booking.cart.ctrl = angular.module('Teatrall.site.states.main.tickets.booking.cart.ctrl', [
        'ui.router',
        'LocalStorageModule',
        'angular-yandex-metrika',
        Teatrall.core.utils.signal.name,
        Teatrall.core.utils.scroll.name,
        Teatrall.core.utils.ab.name,
        Teatrall.core.models.cart.name,
        Teatrall.core.models.analyzableTicket.name,
        Teatrall.core.models.order.name
    ]);

    /**
     *
     * @param $scope
     * @param $q
     * @param $state
     * @param $timeout
     * @param Cart
     * @param Ticket
     * @param Order
     * @params Signal
     * @params $filter
     * @params YandexMetrika
     * @params order
     * @params Scroll
     * @params abTest
     * @ngInject
     */
    var controller = function ($scope, $q, $state, $timeout, localStorageService, Cart, AnalyzableTicket, Signal, $filter, YandexMetrika, order, Scroll, abTest) {
        var self = this;

        this.timeout_ = $timeout;
        this.state_ = $state;
        this.Signal_ = Signal;
        this.filter_ = $filter;
        this.YandexMetrika_ = YandexMetrika;
        this.Scroll_ = Scroll;

        this.newOrder = order;

        this.showErrors = false;
        this.existsUser = false;
        this.showEmpyInputs = true;
        localStorageService.set('sherlock_trigger_schedule_attemt', 0);
        var style = abTest('order_type', ['long_version', 'classic'], 1);

        Cart.get({userId: 'current'},
            function (response) {
                self.cart = response;

                self.counter = self.cart.expires_in;
                self.stopCountdown();
                self.countdown();

                if(response.schedules.length) {
                    self.YandexMetrika_.reachGoal('GO_TO_CART');
                }
                if (style == 'long_version') {
                    self.Scroll_('breadcrumbsAnchor', 400);
                };
            }
        );

        this.order = {
            deliveryType: 'pickup',
            paymentType: 'post_cash'
        };

        this.deliveryTypeInfo = {
            'courier_moscow': {
                'info': 'Доставка в пределах МКАД',
                'price': 250,
            },
            'courier_new_moscow': {
                'info': 'Доставка в районы Москвы за МКАД',
                'price': 450,
            },
            'courier_country': {
                'info': 'Доставка в ближайшее Подмосковье',
                'price': 550,
            }
        }

        this.emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        $scope.$on('$destroy', function () {
            self.stopCountdown();
        });
    }

    controller.prototype.stopCountdown = function () {
        if (this.timer) {
            this.timeout_.cancel(this.timer);
        }
    };


    controller.prototype.countdown = function () {
        var self = this;

        this.timer =  this.timeout_(function() {
            self.counter--;

            if(self.counter != 0) {
                self.countdown();
            }
        }, 1000);
    };

    controller.prototype.validateRequiredEmail = function (input) {
        return input ? input.match(this.emailFormat) : false;
    }

    controller.prototype.validateNotRequiredEmail = function (input) {
        return input ? input.match(this.emailFormat) : true;
    }

    controller.prototype.eventsCount = function () {
        var count = 0;

        angular.forEach(this.cart.schedules, function (schedule) {
            if (schedule.tickets.length > 0) {
                count++;
            }
        });

        return count;
    }

    controller.prototype.calculateTicketsPrice = function () {
        var ticketsPrice = 0;

        angular.forEach(this.cart.schedules, function (schedule) {
            angular.forEach(schedule.tickets, function (ticket) {
                ticketsPrice += parseInt(ticket.price);
            });
        });

        return ticketsPrice;
    }

    controller.prototype.calculateTicketsFee = function () {
        var ticketsPrice = 0;

        angular.forEach(this.cart.schedules, function (schedule) {
            angular.forEach(schedule.tickets, function (ticket) {
                ticketsPrice += parseFloat(ticket.fee) * parseInt(ticket.price);
            });
        });

        return ticketsPrice;
    }

    controller.prototype.calculateTotalPrice = function () {
        return this.calculateTicketsPrice() + this.calculateTicketsFee();
    }

    controller.prototype.calculateOrderPrice = function () {
        var totalPrice = this.calculateTotalPrice();

        if (this.order.deliveryType.indexOf('courier') >= 0) {
            totalPrice += this.deliveryTypeInfo[this.order.deliveryType].price;
        }

        return totalPrice;
    }

    controller.prototype.isEticketPossible = function () {
        if (!this.cart || this.cart.schedules.length == 0) {
            return false;
        }

        for (var i = 0; i < this.cart.schedules.length; i++) {
            var schedule = this.cart.schedules[i];

            if (schedule.tickets.length == 0) {
                return false;
            }

            for (var j = 0; j < schedule.tickets.length; j++) {
                if (!schedule.tickets[j].ticket.is_eticket) {
                    return false;
                }
            }
        }

        return true;
    };

    controller.prototype.isCardPaymentPossible = function () {
        if (!this.cart || this.cart.schedules.length == 0) {
            return false;
        }

        for (var i = 0; i < this.cart.schedules.length; i++) {
            var schedule = this.cart.schedules[i];

            if (schedule.tickets.length > 0 && !schedule.is_card_payment_possible) {
                return false;
            }
        }

        return true;
    }

    controller.prototype.showCart = function () {
        if (!this.cart || this.cart.schedules.length == 0) {
            return false;
        }

        for (var i = 0; i < this.cart.schedules.length; i++) {
            if (this.cart.schedules[i].tickets.length > 0) {
                return true;
            }
        }

        return false;
    };

    controller.prototype.signIn = function () {
        this.Signal_('modal.auth:open');
    }

    controller.prototype.registration = function () {
        this.Signal_('modal.registration:open');
    }

    controller.prototype.createOrder = function () {
        var self = this;

        this.newOrder.id = void 0;

        if (this.order.deliveryType == 'pickup') {
            this.newOrder.first_name = this.order.fullName;
            this.newOrder.phone = this.order.phone;
            this.newOrder.email = this.order.email;
            this.newOrder.delivery_type = this.order.deliveryType;
            this.newOrder.payment_type = this.order.paymentType;
        } else if (this.order.deliveryType == 'eticket') {
            this.newOrder.first_name = this.order.firstName;
            this.newOrder.last_name = this.order.lastName;
            this.newOrder.phone = this.order.phone;
            this.newOrder.email = this.order.email;
            this.newOrder.delivery_type = this.order.deliveryType;
            this.newOrder.payment_type = this.order.paymentType;
        } else if (this.order.deliveryType.indexOf('courier') >= 0) {
            this.newOrder.first_name = this.order.fullName;
            this.newOrder.address = this.order.address;
            this.newOrder.phone = this.order.phone;
            this.newOrder.email = this.order.email;
            this.newOrder.delivery_type = this.order.deliveryType;
            this.newOrder.payment_type = this.order.paymentType;
        }

        var trafficData = sbjs.get;
        if (trafficData) {
            this.newOrder.traffic_data = trafficData;
        } else {
            this.newOrder.traffic_data = {};
        }
        this.newOrder.traffic_data.order_type = 'classic';

        this.newOrder.$save(
            function () {
                self.Signal_('cart-update');
                console.log('Order create success');

                var goods = [];

                angular.forEach(self.newOrder.cart.schedules, function (schedule){
                    var date = new Date(schedule.schedule.date);
                    var name = schedule.schedule.performance.name + ' ' + self.filter_('date')(date, 'dd.MM.yyyy');

                    angular.forEach(schedule.tickets, function (ticket){
                        var place = (ticket.ticket.place.section.name ? ticket.ticket.place.section.name + ' ' : '') +
                                    (ticket.ticket.place.row ? 'ряд ' + ticket.ticket.place.row + ' ': '') +
                                    (ticket.ticket.place.place ? 'место ' + ticket.ticket.place.place + ' ': '');

                        goods.push({
                            id: ticket.ticket.id,
                            price: parseFloat(ticket.price),
                            quantity: 1,
                            name: name + ' (' + place + ')'
                        })
                    });
                });

                self.YandexMetrika_.reachGoal('MAKE_ORDER', {
                    order_id: self.newOrder.id,
                    order_price: parseFloat(self.newOrder.delivery_price) + parseFloat(self.newOrder.payment_price) +
                                 parseFloat(self.newOrder.tickets_price) + parseFloat(self.newOrder.total_fee),
                    currency: "RUR",
                    exchange_rate: 1,
                    goods: goods
                });

                if (self.order.paymentType == "card") {
                    self.state_.go('main.tickets.booking.payment', {id: self.newOrder.id});
                } else {
                    self.state_.go('main.tickets.booking.congratulations', {id: self.newOrder.id});
                }
            },
            function (error) {
                console.log('Order create error')
                if (error.data.ERROR_CODE == -1041) {
                    console.log(error.data.ERROR_CODE);
                    self.showErrors = true;
                    self.existsUser = true;
                }
            }
        )
    };

    controller.prototype.checkErrors = function (formInvalid) {
        var self = this;
        if (formInvalid) {
            self.showErrors = true;
            self.Scroll_('order-step-three', 400);
        } else {
            self.showErrors = false;
            self.createOrder();
        }
    }
    controller.prototype.confirmOrder = function () {
        var self = this;
        self.YandexMetrika_.reachGoal('START_ORDER');
        self.showOrderDetails = true;
        setTimeout(function(){
            self.Scroll_('order-step-one', 400);
        }, 500);
    }

    Teatrall.site.states.main.tickets.booking.cart.ctrl.controller('BookingCartCtrl', controller);


})(window.angular, window.Teatrall, window.sbjs);
