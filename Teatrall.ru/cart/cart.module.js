goog.provide('Teatrall.site.states.main.tickets.booking.cart.module');

goog.require('libs.angular');
goog.require('libs.angular.ui.router');
goog.require('libs.angular.ui.utils.mask');
goog.require('libs.angular.ui.utils.validate');

goog.require('Teatrall.core.utils.filters');

goog.require('Teatrall.site.components.directives.cardBookingBlock');

goog.require('Teatrall.site.states.main.tickets.booking.cart.ctrl');

goog.require('Teatrall.core.directives.daData');

(function (angular, Teatrall) {
    // 'use strict';

    Teatrall.site.states.main.tickets.booking.cart.module = angular.module('Teatrall.site.states.main.tickets.booking.cart.module', [
        'ui.router',
        'ui.mask',
        'ui.validate',
        Teatrall.core.utils.filters.name,
        Teatrall.site.components.directives.cardBookingBlock.name,
        Teatrall.site.states.main.tickets.booking.cart.ctrl.name,
        Teatrall.core.directives.daData.name
    ]);

    /**
     * @ngInject
     */
    var configuration = function ($stateProvider) {
        $stateProvider.state('main.tickets.booking.cart', {
            url: 'cart/',
            templateUrl: '/states/main/tickets/booking/cart/cart.tpl',
            controller: 'BookingCartCtrl as BookingCartCtrl'
        });
    };

    Teatrall.site.states.main.tickets.booking.cart.module.config(configuration);

})(window.angular, window.Teatrall);
