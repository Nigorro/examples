<div>
    <section class="container">
        <div class="row">
            <div class="col-lg-16 col-lg-offset-3">
                <article class="photo-project">
                    <header class="photo-project__header">
                        <div class="">
                            <h1 class="photo-project__title" once-text="PhotoProjectCtrl.post.title"></h1>
                            <p class="photo-project__sub-title" once-text="PhotoProjectCtrl.post.subtitle"></p>
                        </div>
                        <div share></div>
                    </header>
                </article>
            </div>
        </div>
    </section>
    <div class="background-gray">
        <section class="container">
            <div class="row">
                <div class="col-lg-18 col-lg-offset-3">
                    <article class="photo-project" ng-class="{'photo-project--small': PhotoProjectCtrl.isSmall}">
                        <div class="project-carousel">
                            <div class="project-carousel__wrapepr project-carousel__wrapepr--image">
                                <div class="full-width-carousel" ng-style="{ 'left' : PhotoProjectCtrl.currentImageSlide, 'width': PhotoProjectCtrl.carouselImageLength}">
                                    <ul class="carousel-container carousel-container--image">
                                        <li class="carousel-container__item"
                                            ng-repeat="photo in PhotoProjectCtrl.post.photoproject_photos"
                                            ng-class="{'carousel-container__item--last': PhotoProjectCtrl.showLastSlide && $index == PhotoProjectCtrl.post.photoproject_photos.length - 1}">
                                            <div ng-if="PhotoProjectCtrl.showLastSlide && $index == PhotoProjectCtrl.post.photoproject_photos.length - 1" class="carousel-container-last">
                                                <p ng-click="PhotoProjectCtrl.toStart()" class="carousel-container-last__text carousel-container-last__text--arrow">ещё<br>раз</p>
                                                <div class="carousel-container-last__share">
                                                    <p class="carousel-container-last__text carousel-container-last__text--small">Поделись с друзьями</p>
                                                    <div class="share--white" share></div>
                                                </div>
                                            </div>
                                            <img class="carousel-container__image" src="{{photo.photo.original}}">
                                        </li>
                                    </ul>
                                </div>
                                <div class="project-carousel__controls">
                                    <button class="project-carousel__controls-left button button--link" ng-if="PhotoProjectCtrl.currentIndex > 0 && PhotoProjectCtrl.currentIndex != PhotoProjectCtrl.post.photoproject_photos.length" ng-click="PhotoProjectCtrl.prev()">
                                        <i class="icon-right-arrow"></i>
                                    </button>
                                    <button  class="project-carousel__controls-right button button--link" ng-if="PhotoProjectCtrl.currentIndex != PhotoProjectCtrl.post.photoproject_photosPhoto.length" ng-click="PhotoProjectCtrl.next()">
                                        <i class="icon-left-arrow"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        <div class="row relative">
            <div class="project-carousel__legend">
                <ul ng-if="!PhotoProjectCtrl.showLastSlide" class="project-carousel__legend-list">
                    <li class="project-carousel__legend-list-item" 
                    ng-class="{'project-carousel__legend-list-item--active': $index == PhotoProjectCtrl.currentIndex}" ng-click="PhotoProjectCtrl.toSlide($index)" ng-repeat="photo in PhotoProjectCtrl.post.photoproject_photos">
                        <div class="project-carousel__legend-list-item__image-wraper">
                            <img src="{{photo.photo.original}}">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <section class="container">
            <div class="row">
                <div class="col-lg-18 col-lg-offset-3">
                    <article class="photo-project">
                        <div class="project-carousel">
                            <div class="project-carousel__wrapepr project-carousel__wrapepr--content">
                                <div class="full-width-carousel" ng-style="{ 'left' : PhotoProjectCtrl.currentSlide, 'width': PhotoProjectCtrl.carouselLength}">
                                    <ul class="carousel-container carousel-container--text">
                                        <li class="carousel-container__item carousel-container__item--text" ng-repeat="photo in PhotoProjectCtrl.post.photoproject_photos">
                                            <div class="row">
                                                <div class="col-lg-16 col-md-16">
                                                    <p class="carousel-container__item-text">
                                                        {{photo.body}}
                                                    </p>
                                                </div>
                                                <div class="col-lg-8 col-md-8">
                                                    <ul class="carousel-container__text-control">
                                                        <li class="carousel-container__text-control-item carousel-container__text-control-item--icon">
                                                            <i class="icon-heart"
                                                                tooltip-html-compile="<div share class='share--clear'></div>"
                                                                tooltip-placement="top"
                                                                tooltip-append-to-body="false"
                                                                tooltip-trigger="click"></i>
                                                            <!-- <div class="carousel-container__text-control-item-tooltip tooltip--top">
                                                                <div share class="share--clear"></div>
                                                            </div> -->
                                                        </li>
                                                        <li class="carousel-container__text-control-item carousel-container__text-control-item--icon" ng-click="PhotoProjectCtrl.open()">
                                                            <i class="icon-full-screen"></i>
                                                        </li>
                                                        <li class="carousel-container__text-control-item carousel-container__text-control-item--text">{{$index + 1}} / {{PhotoProjectCtrl.post.photoproject_photos.length}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
    <section class="container">
        <div class="row">
            <div class="col-lg-12 col-lg-offset-3">
                <h2 class="photo-project__section-sub-title">{{PhotoProjectCtrl.post.subtitle}}</h2>
                <div  id="text_content" hyphenator ng-bind-html="PhotoProjectCtrl.post.text | toTrusted"></div>
            </div>
        </div>
    </section>
</div>