<section class="container">
    <div class="row">
        <div class="col-lg-18 col-lg-offset-3 col-md-24">
            <div carousel interval="3000" class="photo-project-popup">
                <i class="photo-project-popup__close icon-remove" ng-click="$close()"></i>
                <slide class="photo-project-popup__slider" ng-repeat="photo in PhotoProjectCtrl.post.photoproject_photos" > 
                    <img style="width:100%;" class="photo-project-popup__img"src="{{photo.photo.original}}">
                </slide>
            </div>
        </div>
    </div>
</section>
