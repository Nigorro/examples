<div>
    <section class="container">
        <div class="row">
            <div class="col-lg-16 col-lg-offset-3">
                <article class="photo-project">
                    <header class="photo-project__header">
                        <div class="">
                            <h1 class="photo-project__title" once-text="PhotoProjectCtrl.post.title"></h1>
                            <p class="photo-project__sub-title" once-text="PhotoProjectCtrl.post.subtitle"></p>
                        </div>
                        <div share></div>
                    </header>
                </article>
            </div>
        </div>
    </section>
    <div class="background-gray">
        <section class="container">
            <div class="row relative">
                <div ng-if="PhotoProjectCtrl.showLastSlide && PhotoProjectCtrl.currentIndex == PhotoProjectCtrl.post.photoproject_photos.length - 1" class="col-lg-18 col-lg-offset-3 col-md-24 project-carousel-view-box-last-overlay">
                    <div class="carousel-container-last carousel-container-last--top">
                        <p ng-click="PhotoProjectCtrl.toStart()" class="carousel-container-last__text carousel-container-last__text--arrow">ещё<br>раз</p>
                        <div class="carousel-container-last__share">
                            <p class="carousel-container-last__text carousel-container-last__text--small">Поделись с друзьями</p>
                            <div class="share--white" share></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-lg-offset-3 col-md-16">
                    <div class="project-carousel-view-box">
                        <img class="project-carousel-view-box__image" src="{{PhotoProjectCtrl.post.photoproject_photos[PhotoProjectCtrl.currentIndex].photo.original}}">
                        <div class="project-carousel__controls">
                            <button class="project-carousel__controls-left button button--link" ng-if="PhotoProjectCtrl.currentIndex > 0 && PhotoProjectCtrl.currentIndex != PhotoProjectCtrl.post.photoproject_photos.length" ng-click="PhotoProjectCtrl.prev()">
                                <i class="icon-right-arrow"></i>
                            </button>
                            <button  class="project-carousel__controls-right button button--link" ng-if="PhotoProjectCtrl.currentIndex != PhotoProjectCtrl.post.photoproject_photos.length" ng-click="PhotoProjectCtrl.next()">
                                <i class="icon-left-arrow"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8">
                    <div class="project-carousel-text-container">
                        <ul class="project-carousel-text-container__info">
                            <li class="project-carousel-text-container__info-item project-carousel-text-container__info-item--icon">
                                <i class="icon-heart"
                                    tooltip-html-compile="<div share class='share--clear'></div>"
                                    tooltip-placement="top"
                                    tooltip-append-to-body="false"
                                    tooltip-trigger="click"></i>
                            </li>
                            <li class="project-carousel-text-container__info-item project-carousel-text-container__info-item--icon" ng-click="PhotoProjectCtrl.open()">
                                <i class="icon-full-screen"></i>
                            </li>
                            <li class="project-carousel-text-container__info-item project-carousel-text-container__info-item--text">
                                {{PhotoProjectCtrl.currentIndex + 1}} / {{PhotoProjectCtrl.post.photoproject_photos.length}}
                            </li>
                        </ul>
                        <div class="project-carousel-text-container__description">
                            <p ellipsis>{{PhotoProjectCtrl.post.photoproject_photos[PhotoProjectCtrl.currentIndex].body}}</p>
                        </div>
                        <div class="project-carousel-list">
                            <div carousel interval="30000" class="carousel--cart">
                                <slide ng-repeat="($fIndex, photo) in PhotoProjectCtrl.post.photoproject_photos" ng-if="!($fIndex % 18)"> 
                                    <ul class="project-carousel-list__legent">
                                        <li class="project-carousel-list__legent--item" ng-repeat="($sIndex, item) in PhotoProjectCtrl.post.photoproject_photos.slice($index, $index + 18)" 
                                        ng-class="{'project-carousel-list__legent--item-active': ($fIndex + $sIndex) == PhotoProjectCtrl.currentIndex}"
                                        ng-click="PhotoProjectCtrl.currentIndex = ($fIndex + $sIndex)">
                                            <div class="project-carousel-list__legent--item__image-wrapper">
                                                <img class="project-carousel-list__legent--item-image" src="{{PhotoProjectCtrl.post.photoproject_photos[$fIndex + $sIndex].photo.original}}">

                                            </div>
                                        </li>
                                    </ul>
                                </slide>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="container">
        <div class="row">
            <div class="col-lg-12 col-lg-offset-3">
                <h2 class="photo-project__section-sub-title">{{PhotoProjectCtrl.post.subtitle}}</h2>
                <div  id="text_content" hyphenator ng-bind-html="PhotoProjectCtrl.post.text | toTrusted"></div>
            </div>
        </div>
    </section>
</div>