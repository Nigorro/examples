goog.provide('Teatrall.site.components.directives.photoProject');

goog.require('libs.angular');
goog.require('libs.angular.ui.bootstrap');

goog.require('Teatrall.site.components.directives.share');
goog.require('Teatrall.core.directives.hyphenator');

(function (angular,Teatrall) {
    Teatrall.site.components.directives.photoProject = angular.module('Teatrall.site.components.directives.photoProject', [
            'ui.bootstrap',
            Teatrall.site.components.directives.share.name,
            Teatrall.core.directives.hyphenator.name,
        ]);
    

    /**
     * @ngInject
     */
    var controller = function ($scope, $modal, $interval) {

        this.scope_ = $scope;
        this.interval_ = $interval || 0;
        this.modal_ = $modal;
        this.post = $scope.photoProject;
        this.interval = $scope.interval;

        this.blockWidth = -866;
        this.smallBlockWidth = 640;
        this.currentIndex = 0;
        this.currentSlide = 0;
        this.currentImageSlide = 0;
        this.showLastSlide = false;
        this.isSmall = false;
        this.carouselLength = 866 * this.post.photoproject_photos.length + 1;
        this.carouselImageLength = 866 * this.post.photoproject_photos.length + 1;

        var that = this;
        
        if (this.interval) {
            var slideInterval = $interval(function() {
                that.next();
            }, this.interval);
        };

        $scope.$on('$destroy', function() {
            $interval.cancel(slideInterval);
            self.slideInterval = undefined;
        });
    }

    controller.prototype.prev = function () {
        var self = this;

        self.currentIndex--;

        self.changeSlide();
    }

    controller.prototype.next = function () {
        var self = this;
        if (self.currentIndex + 1 == self.post.photoproject_photos.length) {
            self.showLastSlide = true;
        } else {
            self.currentIndex++;
            self.changeSlide();
        }
    }

    controller.prototype.toStart = function () {
        var self = this;
        this.currentIndex = 0;
        this.currentSlide = 0;
        this.currentImageSlide = 0;
        self.showLastSlide = false;
    }

    controller.prototype.toSlide = function (index) {
        var self = this;
        self.currentIndex = index;
        self.showLastSlide = false;
        self.currentSlide = self.currentIndex * self.blockWidth
        if (self.isSmall) {
            self.currentImageSlide = self.currentIndex * (-self.smallBlockWidth);
        } else {
            self.currentImageSlide = self.currentSlide;
        }

    }

    controller.prototype.changeSlide = function () {
        var self = this;

        if (self.isSmall) {
            self.currentImageSlide  = self.currentIndex * (-self.smallBlockWidth);
            self.currentSlide = self.currentIndex * self.blockWidth
        } else {
            self.currentSlide = self.currentIndex * self.blockWidth;
            self.currentImageSlide = self.currentSlide;
        }
    }

    controller.prototype.changeSize = function () {
        var self = this;

        if (self.isSmall) {
            self.carouselImageLength = self.smallBlockWidth * self.post.photoproject_photos.length + 1;
            self.currentImageSlide  = self.currentIndex * (-self.smallBlockWidth);
        } else {
            self.carouselImageLength = self.carouselLength;
            self.currentImageSlide = self.currentSlide;
        }

        self.changeSlide();

    }
    controller.prototype.open = function () {
        var self = this;

        self.modal_.open({
            templateUrl: '/components/directives/photoProject/popupTemplate.tpl',
            scope: self.scope_,
        });
    }

    var directiveFullwidth = function () {
        return {
            restrict: 'EA',
            controller: 'PhotoProjectCtrl as PhotoProjectCtrl',
            templateUrl: '/components/directives/photoProject/fullwidthTemplatet.tpl',
            replace: true,
            scope: {
                photoProject: '=photoProjectFullwidth',
                interval: '@interval'
            },
            link: function (scope, element, attrs, PhotoProjectCtrl) {

                var windows = angular.element(window);
                
                scope.getWindowDimensions = function () {
                    return {
                        'h': windows.height(),
                        'w': windows.width()
                    };
                };
                scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                    scope.windowHeight = newValue.h;
                    scope.windowWidth = newValue.w;
                    if (scope.windowWidth <= 1280) {
                        PhotoProjectCtrl.isSmall = true;
                        PhotoProjectCtrl.carouselImageLength = PhotoProjectCtrl.smallBlockWidth * PhotoProjectCtrl.post.photoproject_photos.length + 1;
                        PhotoProjectCtrl.changeSize();
                    } else {
                        PhotoProjectCtrl.isSmall = false;

                        // PhotoProjectCtrl.carouselImageLength = 866 * PhotoProjectCtrl.post.photoproject_photos.length + 1;
                        PhotoProjectCtrl.changeSize();
                    }
                }, true);

                windows.bind('resize', function () {
                    scope.$apply();
                });
            }
        }
    };

    var directive = function () {
        return {
            restrict: 'EA',
            controller: 'PhotoProjectCtrl as PhotoProjectCtrl',
            templateUrl: '/components/directives/photoProject/template.tpl',
            replace: true,
            scope: {
                photoProject: '=photoProject',
                interval: '@interval'
            }
        }
    };

    Teatrall.site.components.directives.photoProject.controller('PhotoProjectCtrl', controller);
    Teatrall.site.components.directives.photoProject.directive('photoProjectFullwidth', directiveFullwidth);
    Teatrall.site.components.directives.photoProject.directive('photoProject', directive);

})(window.angular, window.Teatrall);
