# Layouts

### [ Main page](https://www.teatrall.ru/) - [psd](https://yadi.sk/d/CYsX7YEuvFVsG)

### [Booking page](https://www.teatrall.ru/teatr/festival-territoriya/7114-pixel--piksel/2016-10-10-20-00-10635/) - [psd](https://yadi.sk/d/qV3flErzvNkmR)

### [Gallery](https://www.teatrall.ru/post/2991-mashina-myuller-v-gogol-tsentre-repetitsii/) - [psd](https://yadi.sk/d/zwpwOTxrvNkju)

### [Post block](https://www.teatrall.ru/) - [psd](https://yadi.sk/d/RYPGLNnAvNkgY)