<section class="container">
    <div class="row">
        <div class="col-lg-12 col-lg-offset-3">
            <article class="issue">
                <header class="">
                    <div class="">
                        <h1 class="issue__title" once-text="ExaminationDirectiveCtrl.post.title"></h1>
                    </div>
                </header>
            </article>
        </div>
    </div>
</section>
<div class="background-beige">
    <section class="container">
        <div class="row">
            <div class="col-lg-18 col-lg-offset-3">
                <article class="issue">
                    <div class="row">
                        <section class="col-md-16 issue__examination examination" ng-style="{'height': ExaminationDirectiveCtrl.elementHeight}">
                            <div class="examination_questions__container examination_questions__container--animation" ng-show="!ExaminationDirectiveCtrl.showTest">
                                <div  id="text_content" class="issue__text_content" hyphenator ng-bind-html="ExaminationDirectiveCtrl.examination.body | toTrusted"></div>
                                <div class="text-center">
                                    <button class="button button--blue issue__start_button" ng-click="ExaminationDirectiveCtrl.toTest()">начать тест</button>
                                </div>
                            </div>
                            <div class="examination_questions__container examination_questions__container--animation" ng-repeat="question in ExaminationDirectiveCtrl.questions"
                                ng-show="$index == ExaminationDirectiveCtrl.slideIndex && ExaminationDirectiveCtrl.showTest && !ExaminationDirectiveCtrl.finishExam">
                                <p class="examination__question" ng-bind-html="question.body"></p>
                                <ul class="examination__list examination_answer">
                                    <li ng-style="{ 'top' : ExaminationDirectiveCtrl.arrowOffsetTop}" class="examination_answer__item--arrow"><i class="icon-left-arrow"></i></li>
                                    <li ng-repeat="answer in question.answers" class="examination_answer__item"
                                        ng-mouseover="ExaminationDirectiveCtrl.answerHover($event)"
                                        ng-click="ExaminationDirectiveCtrl.answer(answer.id)">
                                        <div ng-bind-html="answer.body | toTrusted"></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="examination_questions__container examination_questions__container--animation"
                                ng-show="ExaminationDirectiveCtrl.finishExam">
                                <h2 class="examination_questions__title">{{ ExaminationDirectiveCtrl.result.title }}</h2>
                                <!-- <p class="examination_questions__result">{{ExaminationDirectiveCtrl.answersResult}} правильных ответа из {{ExaminationDirectiveCtrl.totalQuestions}}</p> -->
                                <div class="examination_questions__text"
                                     ng-bind-html="ExaminationDirectiveCtrl.result.body">
                                </div>
                            </div>
                        </section>
                        <div class="col-md-8">
                            <div class="examination_questions__container">
                                <p ng-if="!ExaminationDirectiveCtrl.finishExam && ExaminationDirectiveCtrl.showTest" class="examination_questions__legends">
                                    {{ExaminationDirectiveCtrl.slideIndex + 1}}&nbsp;/&nbsp;{{ExaminationDirectiveCtrl.totalQuestions }}
                                </p>
                                <!-- <p ng-if="ExaminationDirectiveCtrl.finishExam" class="new_examination">ещё тест</p> -->
                                <div ng-if="ExaminationDirectiveCtrl.finishExam" class="examination_questions__share">
                                    <p>Поделись с друзьями</p>
                                    <div  share></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
</div>
