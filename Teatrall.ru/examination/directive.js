goog.provide('Teatrall.site.components.directives.examination');

goog.require('libs.angular');

goog.require('Teatrall.core.directives.hyphenator');

goog.require('Teatrall.core.models.examination.examination');

goog.require('Teatrall.site.components.directives.share');


(function (angular,Teatrall) {
    Teatrall.site.components.directives.examination = angular.module('Teatrall.site.components.directives.examination', [
        Teatrall.core.directives.hyphenator.name,
        Teatrall.core.models.examination.examination.name,
        Teatrall.site.components.directives.share.name
    ]);


    /**
     *
     * @ngInject
     */
     var controller = function ($scope, Examination) {
        this.post = $scope.post();
        this.examination = new Examination(this.post.examination);

        this.baseArrorOffsetTop = -61;
        this.slideIndex = 0;
        this.finishExam = false;
        this.showTest = false;
        this.answersResults = [];

        this.questions = this.examination.questions;

        this.totalQuestions = this.questions.length;
    };

    controller.prototype.answerHover = function (event) {
        var self = this;
        var element = angular.element(event.currentTarget)[0];
        console.log(self.baseArrorOffsetTop, element.offsetTop, (element.clientHeight / 2));
        self.arrowOffsetTop = self.baseArrorOffsetTop + element.offsetTop + (element.clientHeight / 2);

    }

    controller.prototype.answer = function (answer) {
        var self = this;

        this.answersResults.push(answer);

        if (this.slideIndex == (this.questions.length - 1)) {
            this.examination.$submit(this.answersResults).then(
                function (response) {
                    self.result = response.data;
                    console.log(self.result);
                    self.finishExam = true;
                    self.elementHeight = 'auto';
                },
                function () {
                    console.error("Examination result submit");
                }
            );
        } else {
            this.slideIndex = this.slideIndex + 1;
            var element = angular.element(document.getElementsByClassName('examination__list'))[this.slideIndex].children[1];
            console.log(element[1], this.slideIndex);
            self.arrowOffsetTop = self.baseArrorOffsetTop + (element.clientHeight / 2);
        }
    }

    controller.prototype.toTest = function () {
        var self = this;
        self.showTest = true;

        var findElementsMaxHeingh = function () {
            var height = 0;
            var elementHeight;
            var element = angular.element(document.getElementsByClassName('examination_questions__container'));
            for (var i = 0; i < element.length; i++) {
                elementHeight = angular.element(element[i])[0].clientHeight;
                if (elementHeight > height) {
                    height = elementHeight;
                };
            };
            if (height < 250) {
                height = 250;
            }
            return height;
        }
        self.elementHeight = findElementsMaxHeingh();
    }

    /**
     * @ngInject
     */
    var directive = function () {
        return {
            restrict: 'A',
            templateUrl: '/components/directives/examination/template.tpl',
            controller: 'ExaminationDirectiveCtrl as ExaminationDirectiveCtrl',
            scope: {
                post: '&examination',
            }
        }
    };

    Teatrall.site.components.directives.examination.controller('ExaminationDirectiveCtrl', controller);
    Teatrall.site.components.directives.examination.directive('examination', directive);

})(window.angular, window.Teatrall);
