goog.provide('Teatrall.site.states.main.tickets.collectiveList.module');

goog.require('libs.angular');
goog.require('libs.angular.ui.router');

goog.require('Teatrall.core.directives.repeater');
goog.require('Teatrall.core.directives.bounce');
goog.require('Teatrall.site.components.directives.collectiveBlock');
goog.require('Teatrall.site.components.directives.breadcrumbs');
goog.require('Teatrall.site.components.directives.pagination');
goog.require('Teatrall.site.components.directives.pokemon');

goog.require('Teatrall.site.states.main.tickets.collectiveList.ctrl');


(function (angular, Teatrall) {
    // 'use strict';

    Teatrall.site.states.main.tickets.collectiveList.module = angular.module('Teatrall.site.states.main.tickets.collectiveList.module', [
        'ui.router',
        Teatrall.core.directives.repeater.name,
        Teatrall.core.directives.bounce.name,
        Teatrall.site.components.directives.collectiveBlock.name,
        Teatrall.site.components.directives.breadcrumbs.name,
        Teatrall.site.components.directives.pagination.name,
        Teatrall.site.components.directives.pokemon.name,
        Teatrall.site.states.main.tickets.collectiveList.ctrl.name
    ]);

    /**
     * @ngInject
     */
    var config = function ($stateProvider) {

        $stateProvider
            .state('main.tickets.collectiveList', {
                url: 'teatr/?query&order',
                templateUrl: '/states/main/tickets/collectiveList/collectiveList.tpl',
                controller: 'CollectiveListCtrl as CollectiveListCtrl',
                reloadOnSearch : false
            })
            .state('main.tickets.collectiveListKind', {
                url: 'teatr/{kind:[a-zA-Z\\-]+}/?query&order',
                templateUrl: '/states/main/tickets/collectiveList/collectiveList.tpl',
                controller: 'CollectiveListCtrl as CollectiveListCtrl',
                reloadOnSearch : false
            });
    };

    Teatrall.site.states.main.tickets.collectiveList.module.config(config);

})(window.angular, window.Teatrall);
