<section class="container">
    <div class="row">
        <div class="col-lg-18 col-lg-offset-3 col-md-16">
            <div bread-crumbs></div>
            <h2 class="section__title">{{ CollectiveListCtrl.pageTitle }}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-16 col-lg-12 col-lg-offset-3 mb-lg">
            <form class="form">
                <div class="form__group">
                    <div class="input-group input-group--large">
                        <span class="input-group__addon">
                            <i class="icon-lens--small"></i>
                        </span>
                        <input ng-model="CollectiveListCtrl.query"
                               ng-change="CollectiveListCtrl.queryChaged()"
                               type="text" class="input"
                               placeholder="Поиск" bounce/>
                    </div>
                </div>
            </form>
            <ul class="sort">
                <li class="sort__item"
                    ng-class="{'sort__item--active': !(CollectiveListCtrl.params.order == 'popular')}"
                    ng-click="CollectiveListCtrl.setOrder('popular')">
                    <a>по рейтингу</a>
                </li>
                <li class="sort__item"
                    ng-class="{'sort__item--active': !(CollectiveListCtrl.params.order == 'name')}"
                    ng-click="CollectiveListCtrl.setOrder('name')">
                    <a>по алфавиту</a>
                </li>
            </ul>
            <div class="mt-sm" repeater repeater-resource="CollectiveListCtrl.Collective_" repeater-name="collectives"
                 repeater-params="CollectiveListCtrl.params">
                <div ng-repeat="collective in data" collective-block="collective"></div>
            </div>
            <div class="text-center">
                <button repeater-load-more repeater-name="collectives" class="button button--large button--large--font-large">Показать еще</button>
            </div>
        </div>
        <!--div class="col-md-8  col-lg-8">
            <div repeater-paginator repeater-paginator-size="3" repeater-name="collectives"
                 repeater-paginator-template="/components/directives/pagination/template.tpl"></div>
        </div-->
        <div class="col-md-8 col-lg-6">
            <!-- <aside class="panel" ng-if="CollectiveListCtrl.collectiveKinds.length">
                <div class="text-center clear-font-size">
                    <h3 class="panel__title">Найди свой жанр</h3>
                </div>
                <ul class="performance-genre-list">
                    <li class="performance-genre-list__item" ng-repeat="collectiveKind in CollectiveListCtrl.collectiveKinds">
                        <a ui-sref="main.tickets.collectiveListKind({
                            kind: collectiveKind.slug,
                            order: CollectiveListCtrl.searchParams.order,
                        })" ng-click="CollectiveListCtrl.setCollectiveKind(collectiveKind)">{{ collectiveKind.name }}</a></li>
                </ul>
            </aside> -->
            <aside class="mb-lg">
                <div pokemon="banner_right_1" pokemon-style="media"></div>
            </aside>
            <aside class="mb-lg">
                <div pokemon="spec_right_1" pokemon-style="spec_place_carousel"></div>
            </aside>
            <aside class="mb-lg">
                <div pokemon="banner_right_2" pokemon-style="media"></div>
            </aside>
            <aside class="mb-lg">
                <div pokemon="spec_right_2" pokemon-style="spec_place_carousel"></div>
            </aside>
        </div>
    </div>
</section>
