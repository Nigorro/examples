goog.provide('Teatrall.site.states.main.tickets.collectiveList.ctrl');

goog.require('libs.angular');
goog.require('libs.angular.ui.router');

goog.require('Teatrall.core.models.breadCrumbs');
goog.require('Teatrall.core.models.collective');
goog.require('Teatrall.core.models.collectiveKind');

goog.require('Teatrall.core.utils.meta');
goog.require('Teatrall.core.utils.activityWatcher');
goog.require('Teatrall.core.utils.misc');
goog.require('Teatrall.core.utils.scroll');

(function (angular, Teatrall) {
    // 'use strict';

    Teatrall.site.states.main.tickets.collectiveList.ctrl = angular.module('Teatrall.site.states.main.tickets.collectiveList.ctrl', [
        'ui.router',
        Teatrall.core.models.breadCrumbs.name,
        Teatrall.core.models.collective.name,
        Teatrall.core.models.collectiveKind.name,
        Teatrall.core.utils.meta.name,
        Teatrall.core.utils.activityWatcher.name,
        Teatrall.core.utils.scroll.name
    ]);

    /**
     * @ngInject
     */
    var controller = function ($scope, $state, $location, BreadCrumbs, Collective,
                               CollectiveKind, Meta, ActivityWatcher, Scroll) {
        var self = this;

        this.state_ = $state;
        this.location_ = $location;
        this.BreadCrumbs_ = BreadCrumbs;
        this.Collective_ = Collective;
        this.Scroll_ = Scroll;

        this.collectiveListBreadCrumbs = [
            {
                title: 'ТеатрALL',
                url: $state.href('main.tickets.index')
            },
            {
                title: 'Театры'
            }
        ];

        this.collectiveListKindBreadCrumbs = [
            {
                title: 'ТеатрALL',
                url: $state.href('main.tickets.index')
            },
            {
                title: 'Театры',
                url: $state.href('main.tickets.collectiveList')
            }
        ];

        /*
            Set up UI state
         */

        this.setupBreadCrumbsAndTitle();

        ActivityWatcher.startActivity();
        this.collectiveKinds = CollectiveKind.query(
            function () {
                ActivityWatcher.stopActivity();
                if ($state.params.kind) {
                    var collectiveKind = self.collectiveKinds[Teatrall.core.utils.misc.indexOfObjectByProperty(self.collectiveKinds, 'slug', $state.params.kind)]
                    self.setupBreadCrumbsAndTitle(collectiveKind);
                }
                Meta.set({
                    title: 'Репертуар театров Москвы на текущий сезон: описание, отзывы, артисты, расписание – Заказ билетов на Teatrall.ru',
                    description: 'Все театры Москвы на Teatrall.ru. Расписание представлений, описание зала, отзывы, артисты и труппы.',
                    keywords: 'репертуар, театр, Москва',
                    url: $state.href('main.tickets.collectiveList')
                });
            }
        );

        /*
            Fill params
         */

        this.params = {
            page_size: 10,
            partial: 1,
            q: $state.params.query || void 0,
            order: $state.params.order || 'popular',
            kind_slug: $state.params.kind || void 0
        }
        this.searchParams = $state.params;

        /*
            Fill form
         */

        this.query = $state.params.query;

        /*
            Create location change watcher
        */

        $scope.$on('$locationChangeSuccess', function () {
            var searchParams = $location.search();
            self.searchParams = searchParams;

            if (searchParams.query) {
                self.params.q = searchParams.query;
                self.query = searchParams.query;
            } else {
                self.params.q = void 0;
                self.query = void 0;
            }

            if (searchParams.order) {
                self.params.order = searchParams.order;
            } else {
                self.params.order = 'popular';
            }
        });
    };

    controller.prototype.setupBreadCrumbsAndTitle = function (collectiveKind) {
        if (collectiveKind) {
            var title = collectiveKind.title;
            if (!title) {
                title = collectiveKind.name.charAt(0).toUpperCase() + collectiveKind.name.slice(1);
            }

            var breadCrumbs = angular.copy(this.collectiveListKindBreadCrumbs);
            breadCrumbs.push({ title: title });

            this.pageTitle = title;
            this.BreadCrumbs_.set(breadCrumbs);

            this.Scroll_(0);
        } else {
            this.pageTitle = "Театры";
            this.BreadCrumbs_.set(this.collectiveListBreadCrumbs);
        }
    }

    controller.prototype.setOrder = function (order) {
        if (['popular', 'name'].indexOf(order) < 0) {
            order = 'popular';
        }

        this.location_.search('order', order);
    }

    controller.prototype.setCollectiveKind = function (collectiveKind) {
        if (this.state_.current.name == 'main.tickets.collectiveListKind') {
            window.location = this.state_.href(
                'main.tickets.collectiveListKind',
                angular.extend(this.searchParams, {kind: collectiveKind.slug}),
                {absolute: true}
            );
        }
    };

    controller.prototype.queryChaged = function () {
        if (this.query) {
            this.location_.search('query', this.query);
        } else {
            this.location_.search('query', void 0);
        }
    };

    Teatrall.site.states.main.tickets.collectiveList.ctrl.controller('CollectiveListCtrl', controller);


})(window.angular, window.Teatrall);
